import { ServiceProvider } from './../../providers/service/service';
import { LoginProvider } from './../../providers/login/login';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Md5 } from 'ts-md5/dist/md5';
import { Storage } from '@ionic/storage';

import * as moment from 'moment';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {

  usuarioLogin = {
    user : '',
    password: ''
  };



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loginService: LoginProvider,
    public service: ServiceProvider,
    public storage: Storage,
    public menuCtrl: MenuController
  ) {
  }

  ngOnInit() {
    this.menuCtrl.swipeEnable(false);
    console.log('iniciando...');
    this.storage.get('usuario').then(val=> {
      if (val) {
        this.service.usuario = val;
  
        let  acesso = moment(this.service.usuario.dataAcesso + ' ' + this.service.usuario.horaAcesso, 'YYYY-MM-DD HH:mm');
        acesso = acesso.add(this.service.tempoLogado, 'minutes');

        console.log(acesso);
        
        if (moment().isBefore(acesso)) {
          this.iniciaAplicacao();
        } else {

        }

      }
    });
  }

  iniciaAplicacao() {
    this.service.alerta('Seja Bem-vindo!', 'toast-success');
    this.navCtrl.setRoot('HomePage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  logar() {      

    const loading = this.service.createLoading('Logando...');

    const usuario = [{
      username : this.usuarioLogin.user,
      password_clear : (this.usuarioLogin.password)
    }];

    this.loginService.logar(usuario).subscribe(
      data => {
        console.log(data);
        if (data.body['response'] == false) {
          console.log('não autorizado');
          this.service.alerta('Login não autorizado', 'toast-error');
        } else {

          let usuario = {
            dataAcesso : moment().format('YYYY-MM-DD'),
            horaAcesso : moment().format('HH:mm'),
            dados : data.body
          }

          this.storage.set('usuario', usuario);
          this.service.alerta('Login autorizado', 'toast-success');
          this.navCtrl.setRoot('HomePage');
          console.log('autorizado');
        }
      },
      err => {
        console.log(err);
        loading.dismiss();
      },
      () => {
       loading.dismiss();
      }
    );

    
  }

  ionViewWillLeave() {
    this.menuCtrl.swipeEnable(true);
  }
}
