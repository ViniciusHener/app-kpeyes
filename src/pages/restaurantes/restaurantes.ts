import { PerguntasProvider } from './../../providers/perguntas/perguntas';
import { QuestionariosProvider } from './../../providers/questionarios/questionarios';
import { RestaurantesProvider } from './../../providers/restaurantes/restaurantes';
import { ServiceProvider } from './../../providers/service/service';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-restaurantes',
  templateUrl: 'restaurantes.html',
})
export class RestaurantesPage implements OnInit {

  restaurantes = [];
  questionarios = [];
  perguntas = [];
  perguntasOpces = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    public restService: RestaurantesProvider,
    public questService: QuestionariosProvider,
    public pergService: PerguntasProvider
  ) {
  }

  ngOnInit(){
    this.getRestaurantes();
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RestaurantesPage');
  }

  getRestaurantes() {
    const loading = this.service.createLoading('Carregando...');
    loading.present();

    this.restService.getAll()
    .then((data : any[]) => {
        this.restaurantes = data;
        loading.dismiss();
    })
    .catch((e) => {
      console.error(e);
      loading.dismiss();
    });

    this.questService.getAll()
    .then((data : any[]) => {
        this.questionarios = data;
        console.log(this.questionarios);
    })
    .catch((e) => {
      console.error(e);
    });  

    this.pergService.getAll()
    .then((data : any[]) => {
        this.perguntas = data;
        console.log(this.perguntas);
    })
    .catch((e) => {
      console.error(e);
    });  

    this.pergService.getAllOpcoes()
    .then((data : any[]) => {
        this.perguntasOpces = data;
        console.log(this.perguntasOpces);
    })
    .catch((e) => {
      console.error(e);
    });  
  }

  goToQuestionarios(restaurante) {
    this.questService.questionario.sigla = restaurante.sigla;
    this.questService.questionario.restauranteNome = restaurante.nome;

    this.navCtrl.push('QuestionariosPage');
  }

}
