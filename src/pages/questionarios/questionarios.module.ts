import { QuestionariosPage } from './questionarios';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    QuestionariosPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionariosPage),
  ],
})
export class QuestionariosPageModule {}
