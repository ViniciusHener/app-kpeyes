import { PerguntasProvider } from './../../providers/perguntas/perguntas';
import { QuestionariosProvider } from './../../providers/questionarios/questionarios';
import { Component, OnInit } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-questionarios',
  templateUrl: 'questionarios.html',
})
export class QuestionariosPage implements OnInit {

  questionarios = [];
  
  constructor(
    public navCtrl: NavController,
    public service: ServiceProvider,
    public storage: Storage,
    public questService : QuestionariosProvider,
    public pergService: PerguntasProvider
  ) {

  }

   ngOnInit() {
      this.getQuestionarios();
   }

   getQuestionarios() {     
      //aqui irá entrar um loading...
    //  this.questionarios = this.questService.getQuestionarios();
    const loading = this.service.createLoading('Carregando...');
    loading.present();

    this.questService.getAll()
      .then((data : any[]) => {
          this.questionarios = data;
          loading.dismiss();
      })
      .catch((e) => {
        console.error(e);
        loading.dismiss();
    });  
   }

   abrirQuestionario(questionario) {
      const loading = this.service.createLoading('Carregando...');
      loading.present();
      
      this.questService.questionario.questId = questionario.idApi;
      this.pergService.getQuestionarioPorId(questionario.idApi)
      .then((data:any)=> {
        this.questService.questionario.questoes = data;    
        console.log('questionarios do service');        
        console.log(this.questService.questionario);        
        loading.dismiss();
        this.navCtrl.push('FichaQuestionarioPage');
      })
      .catch((e) => {
        console.error(e);
        loading.dismiss();
      });  


     
     
   }

}
