import { RespostasProvider } from './../../providers/respostas/respostas';
import { PerguntasProvider } from './../../providers/perguntas/perguntas';
import { QuestionariosProvider } from './../../providers/questionarios/questionarios';
import { ServiceProvider } from './../../providers/service/service';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-quest-nao-transmit',
  templateUrl: 'quest-nao-transmit.html',
})
export class QuestNaoTransmitPage implements OnInit{

  questionarios = [];
  
  constructor(
    public navCtrl: NavController,
    public service: ServiceProvider,
    public storage: Storage,
    public questService : QuestionariosProvider,
    public respostaService: RespostasProvider
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestNaoTransmitPage');
  }

  ngOnInit(){
    this.getQuestNTransmitidos();
  }

  getQuestNTransmitidos() {
    const loading = this.service.createLoading('Carregando...');
    loading.present();

    this.respostaService.getNaoTransmitidos()
      .then((data : any[]) => {
          this.questionarios = data;
          loading.dismiss();
          console.log('quest SQL', data);
                    
      })
      .catch((e) => {
        console.error(e);
        loading.dismiss();
    });  
  }

  abrirQuestionario() {

  }

}
