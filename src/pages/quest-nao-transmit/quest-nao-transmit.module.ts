import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestNaoTransmitPage } from './quest-nao-transmit';

@NgModule({
  declarations: [
    QuestNaoTransmitPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestNaoTransmitPage),
  ],
})
export class QuestNaoTransmitPageModule {}
