import { Component, ViewChild, trigger, state, style, transition, animate, keyframes   } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-swipe',
  templateUrl: 'swipe.html',
  animations: [    
    trigger('buttonState', [
          state('*', style({
              transform: 'translateX(0)',              
          })),
          transition('* => rightSwipe', animate('600ms ease-out', keyframes([            
            style({transform: 'translateX(0)', offset: 0, opacity: 0}),
            style({transform: 'translateX(-500px)',  offset: 0.3}),
            style({transform: 'translateX(0)',     offset: 1.0, opacity: 1}),            
          ]))),
          transition('* => leftSwipe', animate('600ms ease-out', keyframes([
            style({transform: 'translateX(0)', offset: 0, opacity: 0}),
            style({transform: 'translateX(500px)',  offset: 0.3}),
            style({transform: 'translateX(0)',     offset: 1.0, opacity: 1})
          ])))
      ])
    ]

})
export class SwipePage {
  
  state: string = 'x';

  questionarios = [
    {
      pergunta : 'QUESTÃO 001',
      peso : 10,
      tipo : 1,      
    },
    {
      pergunta : 'QUESTÃO 002',
      peso : 8,
      tipo : 1,      
    },
    {
      pergunta : 'QUESTÃO 003',
      peso : 10,
      tipo : 1,      
      opcao: ['opção 1', 'opção 2', 'opção 3']
    },
    {
      pergunta : 'QUESTÃO 004',
      peso : 10,
      tipo : 1,      
      opcao: ['SIM', 'NAO', 'TALVEZ']
    }
  ];

  index = 0;
  questao : any;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.questao = this.questionarios[this.index];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SwipePage');
  }

  swipeEvent(event) {
    if (!this.questao.resposta) {
      this.questao.resposta = 'NAO SE APLICA';
    }

    if (event.direction == 4) {
      if (this.index > 0) {
        this.index--;
        this.animateLeft();
      }
    } else if (event.direction == 2) {
      if (this.index < this.questionarios.length - 1) {
        this.index++;
        this.animateRight();
      }
    }


    console.log(this.index);
    
    this.questao = this.questionarios[this.index];
    setTimeout(() => {
      this.state = 'x';      
     }, 700);
  }

  changeState() {
    this.state = this.state == 'left' ? 'right': 'left';
  }
  
  animateLeft() {
    this.state = 'rightSwipe';
    // this.animator.setType('slideInLeft').show(this.myElem.nativeElement);
  }
  
  animateRight() {
    this.state = 'leftSwipe';
    // this.animator.setType('slideInRight').show(this.myElem.nativeElement);
  }

  animationDone() {
    this.state = 'x';
  }
  
  responder(questao, resposta) {
    questao.resposta = resposta;
    if (this.index < this.questionarios.length - 1) {
      this.index++;
      this.animateRight();
      setTimeout(() => {
        this.state = 'x';      
      }, 700);
    }
    this.questao = this.questionarios[this.index];
  }

  salvar() {
    
  }

}
