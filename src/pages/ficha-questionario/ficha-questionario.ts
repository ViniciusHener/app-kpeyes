import { RespostasProvider } from './../../providers/respostas/respostas';
import { ServiceProvider } from './../../providers/service/service';
import { PerguntasProvider } from './../../providers/perguntas/perguntas';
import { QuestionariosProvider } from './../../providers/questionarios/questionarios';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { importExpr } from '@angular/compiler/src/output/output_ast';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-ficha-questionario',
  templateUrl: 'ficha-questionario.html',
})
export class FichaQuestionarioPage implements OnInit {

  questionario: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public questService: QuestionariosProvider,
    public storage: Storage,
    public perguntaService: PerguntasProvider,
    public respostaService: RespostasProvider,
    public serivce: ServiceProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FichaQuestionarioPage');
  }

  ngOnInit(){  
    this.questionario = this.questService.questionario.questoes;
    const nome = 'quest'+this.questService.questionario.sigla+this.questService.questionario.questId

    this.storage.get(nome).then((val) => {
      if (val) {
        this.questService.questionario = val;
        this.questionario = this.questService.questionario.questoes;
      }
    });
    
  }

  salvarRespostas() {
    
    this.questService.questionario.questoes = this.questionario;

    let completo = true;
    for (let i = 0; i < this.questionario.length; i++) {
      const questao = this.questionario[i];
      if (!questao.resposta) {
        completo = false;
      }      
    }

    console.log(completo);
    const nome = 'quest'+this.questService.questionario.sigla+this.questService.questionario.questId;
    console.log(nome);
    
    if (completo) {
      this.insereDB(nome);  
    } else {
      this.storage.set(nome, this.questService.questionario);
      this.navCtrl.pop();
    }

    console.log(this.questService.questionario);
    
  }


  insereDB(nome) {

    console.log('INSERINDO NO DB');
    

    const loading = this.serivce.createLoading('Salvando...');
    loading.present();

    this.respostaService.insert(this.questService.questionario)
    .then(() => {
      this.storage.remove(nome);
      loading.dismiss();
      this.navCtrl.pop();
      this.serivce.alerta('Questionário salvo com sucesso!', 'toast-success');
    })
    .catch((e) => {
      console.error(e)
      loading.dismiss();
      this.serivce.alerta('Erro ao salvar questionário', 'toast-error');
    });  


  }
}
