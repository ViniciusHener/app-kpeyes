import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FichaQuestionarioPage } from './ficha-questionario';

@NgModule({
  declarations: [
    FichaQuestionarioPage,
  ],
  imports: [
    IonicPageModule.forChild(FichaQuestionarioPage),
  ],
})
export class FichaQuestionarioPageModule {}
