import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AnimationService, AnimationBuilder } from 'css-animator';

@IonicPage()
@Component({
  selector: 'page-teste',
  templateUrl: 'teste.html',
})
export class TestePage {
  @ViewChild('myCard') myElem;
 
  private animator: AnimationBuilder;

  questoes = [
    {
      pergunta : 'QUESTÃO 001',
      peso : 10,
      tipo : 1,      
    },
    {
      pergunta : 'QUESTÃO 002',
      peso : 8,
      tipo : 2,      
    },
    {
      pergunta : 'QUESTÃO 003',
      peso : 10,
      tipo : 3,      
      opcao: ['opção 1', 'opção 2', 'opção 3']
    },
    {
      pergunta : 'QUESTÃO 004',
      peso : 10,
      tipo : 4,      
      opcao: ['SIM', 'NAO', 'TALVEZ']
    }
  ]

  index = 0;
  questao = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public animationService: AnimationService
  ) {
     this.animator = animationService.builder();
    this.questao = this.questoes[this.index];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestePage');
  }

  swipeEvent(event){
    //console.log(event);
    if (event.direction == 4) {
      if (this.index > 0) {
        this.index--;
        this.animateLeft();
      }
    }

    if (event.direction == 2) {
      if (this.index < this.questoes.length - 1) {
        this.index++;
        this.animateRight();
      }
    }

    this.questao = this.questoes[this.index];
    console.log(this.index);
    
  }

  animateLeft() {
    this.animator.setType('slideInLeft').show(this.myElem.nativeElement);
  }

  animateRight() {
    this.animator.setType('slideInRight').show(this.myElem.nativeElement);
  }
 
}
