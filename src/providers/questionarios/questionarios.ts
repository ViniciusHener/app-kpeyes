import { PerguntasProvider } from './../perguntas/perguntas';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as moment from 'moment';
import { DatabaseProvider } from '../database/database';
import { SQLiteObject } from '@ionic-native/sqlite';
import { ServiceProvider } from '../service/service';

@Injectable()
export class QuestionariosProvider {

  questionario = {
    sigla : null,
    restauranteNome: '',
    usuario : null,
    questId: null,
    data: moment().format('YYYY-MM-DD'),
    hora: moment().format('HH:mm'),
    questoes: []
  };

  

  /*
  1 - texto
  2 - numero
  3 - marcação
  4 - opção
  */

  constructor(public http: HttpClient,
    public dbService : DatabaseProvider,
    public service: ServiceProvider,
    public pergService: PerguntasProvider
    ) {
    console.log('Hello QuestionariosProvider Provider');
  }

  public insert(questionario) {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'replace into questionarios (idApi, questionario, tipo) values (?, ?, ?)';
      let dados = [questionario.id, questionario.descricao, questionario.codigo];

      return db.executeSql(sql, dados)
      .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e));  
  }

  temp(id) : any {    
    let tempQuest = [];
    this.pergService.getQuestionarioPorId(id)
    .then(
      (data :any) => {
          this.questionario = data;
          return tempQuest;
      }
    )
    .catch((e) => {
      console.error(e);
      return tempQuest;
    });  
  }

  public getAll() {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'select * from questionarios';
      let dados = [];

      return db.executeSql(sql, dados)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let questionarios = [];
            for (let i = 0; i < data.rows.length; i++) {
              const questionario = data.rows.item(i);
              questionarios.push(questionario);
            }
            return questionarios;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e)); 
  }

  

  getQuestionarioApi() {
    let headers = new HttpHeaders();
    this.service.createAuthorizationHeader(headers);    
    return this.http.get(this.service.servidor + 'json&format=json&Itemid=806', {headers : headers, observe: 'response'});  
  }

}
