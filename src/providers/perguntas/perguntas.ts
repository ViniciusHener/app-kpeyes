import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { ServiceProvider } from '../service/service';
import { SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the PerguntasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PerguntasProvider {

  constructor(public http: HttpClient,
    public dbService : DatabaseProvider,
    public service: ServiceProvider) {
    console.log('Hello PerguntasProvider Provider');
  }

  public insert(pergunta) {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'replace into perguntas (idApi, idQuest, sigla, pergunta, peso, tipo) values (?, ?, ?, ?, ?, ?)';
      let dados = [pergunta.id, pergunta.id_questionario, 
        pergunta.sigla, pergunta.pergunta, 
        pergunta.peso, pergunta.tipo];

      return db.executeSql(sql, dados)
      .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e));  
  }

  public insertOpces(id, respostas) {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'replace into opcoesPerguntas (idPergunta, opcao) values (?, ?)';
      let dados = [id, respostas];

      return db.executeSql(sql, dados)
      .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e));  
  }

  public getAll() {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'select * from perguntas';
      let dados = [];

      return db.executeSql(sql, dados)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let perguntas = [];
            for (let i = 0; i < data.rows.length; i++) {
              const pergunta = data.rows.item(i);
              perguntas.push(pergunta);
            }
            return perguntas;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e)); 
  }

  public getQuestionarioPorId(id) {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'select t1.*, t2.opcao from perguntas AS t1 left join opcoesPerguntas AS t2 ON (t2.idPergunta = t1.idApi) WHERE t1.idQuest = ?';
      let dados = [id];

      return db.executeSql(sql, dados)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let perguntas = [];
            for (let i = 0; i < data.rows.length; i++) {
              const pergunta = data.rows.item(i);
              
              let opcao = pergunta.opcao;
              if (opcao) {
                opcao = opcao.split(',');
                pergunta.opcao = opcao;
              }

              perguntas.push(pergunta);
            }            
            return perguntas;
          } else {            
            return [];
          }
        })
        .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e)); 
  }

  public getAllOpcoes() {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'select * from opcoesPerguntas';
      let dados = [];

      return db.executeSql(sql, dados)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let opcoesPerguntas = [];
            for (let i = 0; i < data.rows.length; i++) {
              const opcoesPergunta = data.rows.item(i);
              opcoesPerguntas.push(opcoesPergunta);
            }
            return opcoesPerguntas;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e)); 
  }

  getPerguntaApi() {
    let headers = new HttpHeaders();
    this.service.createAuthorizationHeader(headers);    
    return this.http.get(this.service.servidor + 'json&format=json&Itemid=779', {headers : headers, observe: 'response'});  
  }

}
