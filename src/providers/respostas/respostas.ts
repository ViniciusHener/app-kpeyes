import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { ServiceProvider } from '../service/service';
import { SQLiteObject } from '@ionic-native/sqlite';

import * as moment from 'moment';

@Injectable()
export class RespostasProvider {

  constructor(public http: HttpClient,
    public dbService : DatabaseProvider,
    public service: ServiceProvider) {
    console.log('Hello RespostasProvider Provider');
  }

  
  public insert(resposta) {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
    
      console.log('respostaInsert',resposta);
      let sql = 'insert into respostas (idQuest, idPergunta, sigla, idUser, dataResp, resposta, transmitido) values (?, ?, ?, ?, ?, ?, ?)';
      
      for (let i = 0; i < resposta.questoes.length; i++) {
        const questao = resposta.questoes[i];
        
        let dados = [
          resposta.questId,
          questao.idApi,
          resposta.sigla,
          resposta.usuario,
          moment(resposta.data + ' ' + resposta.hora, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm:ss'),
          questao.resposta,
          0
        ];

        db.executeSql(sql, dados)
        .then(()=> {
          if ( i == resposta.questoes.length - 1) {
            return 1;
          }
        })
        .catch((e) => console.error(e));  

      }
      

    })
    .catch((e) => console.error(e));  
  }


  public getNaoTransmitidos() {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'select distinct t1.idQuest, t1.sigla, t1.dataResp, t1.transmitido, t2.questionario, t3.nome AS restaurante '+
      'from respostas AS t1 '+
      'INNER JOIN questionarios AS t2 ON (t1.idQuest = t2.idApi)' + 
      'INNER JOIN restaurantes AS t3 ON (t1.sigla = t3.sigla)' + 
      'where transmitido = 0';      
      
      let dados = [];

      return db.executeSql(sql, dados)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let perguntas = [];
            for (let i = 0; i < data.rows.length; i++) {
              const pergunta = data.rows.item(i);
              perguntas.push(pergunta);
            }
            return perguntas;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e)); 
  }

  public getAll() {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'select * from respostas';
      
      let dados = [];

      return db.executeSql(sql, dados)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let respostas = [];
            for (let i = 0; i < data.rows.length; i++) {
              const resposta = data.rows.item(i);    
              respostas.push(resposta);
            }
            return respostas;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e)); 
  }
  

}
