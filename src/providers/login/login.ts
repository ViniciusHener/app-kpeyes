import { ServiceProvider } from './../service/service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider {



  constructor(
    public http: HttpClient,
    public service: ServiceProvider

  ) {
    console.log('Hello LoginProvider Provider');
  }

  logar(usuario) {
    let headers = new HttpHeaders();
    this.service.createAuthorizationHeader(headers);
    return this.http.post(this.service.servidor + 'auth&format=json&Itemid=586', usuario, {headers : headers, observe: 'response'});
  }


}
