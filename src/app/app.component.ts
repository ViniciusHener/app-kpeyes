import { PerguntasProvider } from './../providers/perguntas/perguntas';
import { QuestionariosProvider } from './../providers/questionarios/questionarios';
import { ServiceProvider } from './../providers/service/service';
import { RestaurantesProvider } from './../providers/restaurantes/restaurantes';
import { DatabaseProvider } from './../providers/database/database';
import { Storage } from '@ionic/storage';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { ListPage } from '../pages/list/list';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'LoginPage';
  
  loading: any;
  
  pages: Array<{title: string, component: any}>;
  listaIcones : Array<string>;
  listaCores : Array<string>;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public storage: Storage,
    public dbService : DatabaseProvider,
    public restService: RestaurantesProvider,
    public service: ServiceProvider,
    public questService: QuestionariosProvider,
    public pergService: PerguntasProvider,
    private network: Network,
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage'},
      { title: 'Questionários', component: 'RestaurantesPage'},
      { title: 'Questionarios não transmitidos', component: 'QuestNaoTransmitPage'},
      { title: 'SWIPE', component: 'SwipePage'}
    ];

    this.listaIcones = ['home','paper','paper'];
    this.listaCores = ['primary','primary', 'primary'];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      
      this.dbService.createDatabase()
      .then(() => {
        // fechando a SplashScreen somente quando o banco for criado
        this.splashScreen.hide();        
      })
      .catch(() => {
        // ou se houver erro na criação do banco        
        this.splashScreen.hide();        
      });


    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logOff() {
    this.storage.remove('usuario');
    this.nav.push('LoginPage');    
  }


  sincRestaurantes() {
    console.log('sinc restaurantes');

    this.restService.getRestauranteApi().subscribe(
      data => {

        let restaurantes : any;
        restaurantes = data.body;
        // console.log(restaurantes);  
        if (restaurantes.length) {
          for (let i = 0; i < restaurantes.length; i++) {
            const restaurante = restaurantes[i];
            this.restService.insert(restaurante)
            .then(() => {
              // console.log('inseriu ' + i);
              if ( i == restaurantes.length-1) {
                this.sincQuestionarios();
                //this.service.alerta('Restaurantes sincronizados com sucesso!', 'toast-success')
              }
            })
            .catch((e) => {
              console.error(e)
              this.loading.dismiss();
            }); 
          }
        }       
      },
      err => {
        console.log(err);        
        this.loading.dismiss();
      },
      ()=> {        
      }
    );
  }


  sincQuestionarios() {

    console.log('sinc questionarios');
    
    this.questService.getQuestionarioApi().subscribe(
      data => {

        let questionarios : any;
        questionarios = data.body;
        // console.log(questionarios);  
        if (questionarios.length) {
          for (let i = 0; i < questionarios.length; i++) {
            const questionario = questionarios[i];
            this.questService.insert(questionario)
            .then(() => {
              // console.log('inseriu ' + i);
              if ( i == questionarios.length-1) {
               this.sincPerguntas();
                // this.service.alerta('Questionários sincronizados com sucesso!', 'toast-success')
              }
            })
            .catch((e) => {
              console.error(e)
              this.loading.dismiss();
            });  
            
          }
        }       
      },
      err => {
        console.log(err);        
        this.loading.dismiss();
      },
      ()=> {      
      }
    );
  }

  sincPerguntas () {

    console.log('sinc perguntas');
    
    this.pergService.getPerguntaApi().subscribe(
      data => {

        let perguntas : any;
        perguntas = data.body;
        // console.log(perguntas);  
        if (perguntas.length) {
          for (let i = 0; i < perguntas.length; i++) {
            const pergunta = perguntas[i];

            let respostaOpcao = null;

            if (perguntas[i].respostas) {
              const respostasOpcoes = JSON.parse(perguntas[i].respostas);
              respostaOpcao = respostasOpcoes.resposta;
            }
            
            this.pergService.insert(pergunta)
            .then(() => {              
              if (respostaOpcao) {
                this.insertOpcoes(pergunta.id, respostaOpcao).then(() => {
                  if ( i == perguntas.length-1) {
                    this.service.alerta('Dados sincronizados com sucesso!', 'toast-success');
                    this.loading.dismiss();
                  }
                })
                .catch((e) => console.error(e));  
              } else {
                if ( i == perguntas.length-1) {
                  this.service.alerta('Dados sincronizados com sucesso!', 'toast-success');
                  this.loading.dismiss();
                }
              }
              
            })
            .catch((e) => {
              console.error(e)
              this.loading.dismiss();
            });  
            
          }
        }       
      },
      err => {
        console.log(err);        
        this.loading.dismiss();
      },
      ()=> {      
      }
    );
  }


  insertOpcoes(id , opcoes) {
    return this.pergService.insertOpces(id, opcoes)
    .then(() => {
      return true;
    })
    .catch((e) => {
      console.error(e)
      this.loading.dismiss();
    });  
  }


  sincronizar() {
    if (this.network.type == 'none') {
      this.service.alerta('Você precisa estar conectado para sincronizar', 'toast-error');
    } else {
      this.loading = this.service.createLoading('Carregando...')
      this.loading.present();
      this.sincRestaurantes();
    }
  }  

  
}
