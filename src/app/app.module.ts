import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { MyApp } from './app.component';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginProvider } from '../providers/login/login';
import { ServiceProvider } from '../providers/service/service';
import { HttpClientModule } from '@angular/common/http';

import { IonicStorageModule } from '@ionic/storage';
import { QuestionariosProvider } from '../providers/questionarios/questionarios';

import { SQLite } from '@ionic-native/sqlite';
import { DatabaseProvider } from '../providers/database/database';
import { RestaurantesProvider } from '../providers/restaurantes/restaurantes';
import { PerguntasProvider } from '../providers/perguntas/perguntas';
import { RespostasProvider } from '../providers/respostas/respostas';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AnimationService, AnimatesDirective } from 'css-animator';

@NgModule({
  declarations: [
    MyApp,
    ListPage,
    AnimatesDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),

    IonicStorageModule.forRoot({
      name: 'kpeyes',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LoginProvider,
    ServiceProvider,
    SQLite,
    QuestionariosProvider,
    DatabaseProvider,
    RestaurantesProvider,
    PerguntasProvider,
    Network,
    RespostasProvider,
    AnimationService
  ]
})
export class AppModule {}
